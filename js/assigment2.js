
    
window.onload= function(){

        //-------------- genrateCanvas function ---------------
        var genrateCanvas = document.getElementById("genrateCanvas");
        var canvasWrp = document.getElementById("canvasWrp");
        var idCount = 0;
        var groupCount = 0;
        var idArry = [] ;
        var onece = 0;
        var onece2 = 0;
        var w = 250;
        var h = 250;

        genrateCanvas.onclick = function(){
         // idArry = [] ;
         // selectIdObject= null;
                    //-------- Property ------------
                    var p = document.createElement("p");
                            groupCount++;
                    let random = Math.floor(Math.random() * 3) + 3;

                    //--------------- Loop append canvas ------
                    for(var c=0; c<random; c++){
                        idCount++;
                        cvs = document.createElement("canvas");
                        ctx = cvs.getContext("2d");
                        cvs.setAttribute("id","cvs_"+idCount);
                        cvs.setAttribute("width","250");
                        cvs.setAttribute("height","250");
                        cvs.cloneNode(true);
                        canvasWrp.appendChild(cvs);
                        pText = document.createTextNode("Group"+groupCount);
                        var idObj = {
                          index:c,
                          idName:"cvs_"+idCount,
                          fabricEle: new fabric.Canvas(cvs),
                          
                        }
                        idArry.push(idObj);
                        
                    }
                    p.appendChild(pText);
                    p.setAttribute("class","cvs-group group-"+ groupCount);
                    canvasWrp.appendChild(p);
                    console.log("Radom canvas generate Count", random,idArry);
                    

                    //------------- ID list append fucntion -------
                    var canvasIdSelect = document.getElementById("canvasIdSelect");
                    function canvasIdList(idList){
                        let len = idList.length;
                        let canvasId = "<option value='null'>Canvas ID</option>";
                        for(var i= 0; i < len; i++){
                            canvasId +="<option value='"+idList[i].index+"'>"+idList[i].idName+"</option>";
                        }
                        canvasIdSelect.innerHTML = canvasId;
                        canvasIdSelect.removeAttribute("disabled"); 
                    };
                    canvasIdList(idArry);

        }


      // ----------------- canvas Id List  slectd   fucntion  -------------------------
        var selectIdElement = null;
        var selectIdElementVal = null;
        var selectIdVal = 'null';
        var selectIdObject = 'null';

        canvasIdSelect.onchange =  function() {
          selectIdElement = null;
          selectIdVal = canvasIdSelect.value;
          selectIdObject = idArry[selectIdVal];
          if(selectIdVal != 'null'){
                shapeSelect.removeAttribute("disabled");
                // ---- remove previous selected id class name remove  ------------
                console.log("Calss length---------------", );
                selectIdElement = document.getElementById(selectIdObject.idName);
                activeIdFun();
                  //canvas =  new fabric.Canvas(selectIdVal);
                }else {
              shapeSelect.setAttribute("disabled",'');
            }
           
            activeObject = selectIdObject.fabricEle.getActiveObject();
            selectIdObject.fabricEle.setActiveObject(selectIdObject.fabricEle.item(0));
            selectIdObject.fabricEle.requestRenderAll();
            selectIdObject.fabricEle.on({
              'object:moving': taferObject,
              'object:scaling': taferObject,
            });
            
        }



        //--------------- Shapes ---------------
        var shapeSelect = document.getElementById("shapeSelect");
        var  selectedShapeVal = 'null';
        shapeSelect.onchange = function(){
             selectedShapeVal =   shapeSelect.value;
        
        }



         //-------------- Genrate Canvas function ---------------
          genrateShape.onclick = function(){
            selectIdObject.fabricEle.on({
              'object:moving': taferObject,
              'object:scaling': taferObject,
            });
            // ------------ Validation for genrate shape  -------------
              if(idArry.length == 0) {
                alert("Please generate Canvas");
              }else if(selectIdVal == 'null'){
                    alert("Please select Canvas ID");
                  }else if(selectedShapeVal == 'null') {
                    alert("Please select shape");
              }
          
                  // ----------- Genrate Shape --------------
              if( idArry.length > 0 && selectIdVal != 'null' && selectedShapeVal != 'null' ) {
                    fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';
                    fabric.Object.prototype.transparentCorners = false;
                    var rect = new fabric.Rect({ width: 79, height: 59, left: 160, top: 160, fill: '#f55', strokeWidth: 1, stroke: 'black' })
                    var triangle = new fabric.Triangle({ width: 40, height: 40, left: 50, top: 150, fill: '#55f', strokeWidth: 1, stroke: 'black' });
                    var circle = new fabric.Circle({ radius: 29.5, left: 120, top: 90, fill: 'rgb(50, 205, 50)', strokeWidth: 1, stroke: 'black' });
                    circle.lockScalingX = circle.lockScalingY = true;
                    triangle.lockScalingX = circle.lockScalingY = true;
                    rect.lockScalingX = circle.lockScalingY = true;


                    if(selectedShapeVal == "rect"){
                      selectIdObject.fabricEle.add(rect);
                      
                    }else if(selectedShapeVal == "triangle"){
                      selectIdObject.fabricEle.add(triangle);
                      
                    }else if(selectedShapeVal == "circle"){
                      selectIdObject.fabricEle.add(circle);
                      
                    }
                  
                
              }

           
         }
         
         
         function activeIdFun() {
              if(typeof  document.getElementsByClassName("id-selecte")[0] !='undefined'){
                document.getElementsByClassName("id-selecte")[0].classList.remove("id-selecte");
               }
               
              document.getElementById(selectIdObject.idName).classList.add("id-selecte");
         }


        // ------------------------ Tanfer Object one canvas to another canvas --------------
        function taferObject() {
         
          var activeObject = selectIdObject.fabricEle.getActiveObject();
          var activeObjectType = activeObject.get('type');
          let activeObjectTop = activeObject.top; 
          let activeObjectLeft = activeObject.left;
          let activeObjectWidth = activeObject.width; 
          let activeObjectHeight = activeObject.height; 
        if(activeObjectTop > 255){
          selectIdObject.fabricEle.remove(activeObject);
            if(selectIdObject.index < idArry.length-1) {
           
              let circle = new fabric.Circle({ radius: 29.5, left: activeObjectLeft, top: activeObjectHeight/2, fill: 'rgb(50, 205, 50)', strokeWidth: 1, stroke: 'black' });
              let triangle = new fabric.Triangle({ width: 40, height: 40, left: activeObjectLeft, top:activeObjectHeight/2, fill: '#55f', strokeWidth: 1, stroke: 'black' });
              let rect = new fabric.Rect({ width: 79, height: 59, left: activeObjectLeft, top: activeObjectHeight, fill: '#f55', strokeWidth: 1, stroke: 'black' })
              
              circle.lockScalingX = circle.lockScalingY = true;
              triangle.lockScalingX = circle.lockScalingY = true;
              rect.lockScalingX = circle.lockScalingY = true;
              selectIdObject= idArry[selectIdObject.index+1];
                      if(activeObjectType == 'circle'){
                        selectIdObject.fabricEle.add(circle);
                      }else if(activeObjectType == 'triangle') {
                        selectIdObject.fabricEle.add(triangle);
                      }else if(activeObjectType == 'rect') {
                        selectIdObject.fabricEle.add(rect);
                      }
                    
                      document.getElementById("canvasIdSelect").selectedIndex = selectIdObject.index +1;


                    selectIdObject.fabricEle.off('object:moving');
                   
                  
                   
                    selectIdObject.fabricEle.setActiveObject(selectIdObject.fabricEle.item(0));
                    activeObject = selectIdObject.fabricEle.getActiveObject();
                    selectIdObject.fabricEle.requestRenderAll();
                    selectIdObject.fabricEle.on({
                      'object:moving': taferObject,
                      'object:scaling': taferObject,
                    });

                    activeIdFun();
           

            }else {

              activeObject.set('top', h-activeObjectHeight);
              selectIdObject.fabricEle.requestRenderAll();
              selectIdObject.fabricEle.off('object:moving');
              selectIdObject.fabricEle.on({
                    'object:moving': taferObject,
                    'object:scaling': taferObject,
                  });

            }
            
          }else if(activeObjectTop < 0){
            
            console.log(activeObjectTop, activeObjectLeft);
              
            let circle = new fabric.Circle({ radius: 29.5, left: activeObjectLeft, top: h-activeObjectHeight, fill: 'rgb(50, 205, 50)', strokeWidth: 1, stroke: 'black' });
            let triangle = new fabric.Triangle({ width: 40, height: 40, left: activeObjectLeft, top: h-activeObjectHeight, fill: '#55f', strokeWidth: 1, stroke: 'black' });
            let rect = new fabric.Rect({ width: 79, height: 59, left: activeObjectLeft, top:  h-activeObjectHeight, fill: '#f55', strokeWidth: 1, stroke: 'black' })
            
            circle.lockScalingX = circle.lockScalingY = true;
            triangle.lockScalingX = circle.lockScalingY = true;
            rect.lockScalingX = circle.lockScalingY = true;
           
                if(selectIdObject.index != 0) {
              
                 
                      if(activeObjectType == 'circle'){
                      idArry[selectIdObject.index-1].fabricEle.add(circle);
                      }else if(activeObjectType == 'triangle') {
                        idArry[selectIdObject.index-1].fabricEle.add(triangle);
                      }else if(activeObjectType == 'rect') {
                        idArry[selectIdObject.index-1].fabricEle.add(rect);
                      }
                  

                      document.getElementById("canvasIdSelect").selectedIndex = selectIdObject.index;
                    selectIdObject.fabricEle.off('object:moving');
                    selectIdObject.fabricEle.remove(activeObject);
                    selectIdObject= idArry[selectIdObject.index-1];
                   
                    selectIdObject.fabricEle.setActiveObject(selectIdObject.fabricEle.item(0));
                    activeObject = selectIdObject.fabricEle.getActiveObject();
                    selectIdObject.fabricEle.requestRenderAll();
                    selectIdObject.fabricEle.on({
                      'object:moving': taferObject,
                      'object:scaling': taferObject,
                    });
                    activeIdFun();

                  }else {
                    activeObject.set('top', "0");
                    selectIdObject.fabricEle.requestRenderAll();
                    selectIdObject.fabricEle.off('object:moving');
                    selectIdObject.fabricEle.on({
                          'object:moving': taferObject,
                          'object:scaling': taferObject,
                        });
                  
                  }

          }

        }
        

    };


