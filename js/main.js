
    
window.onload= function(){
    
    // ------------ Http call fucntion --------
    var urlTodos = "https://jsonplaceholder.typicode.com/todos";
    var todoData;
    var spesificData;
        
        function httpCall(url){
            let table_body = document.getElementById("todoTable_body");
            const http = new XMLHttpRequest();
                    http.open("GET", url, true);
                    http.onprogress = function () {
                        table_body.innerHTML="<td class='loding-data' colspan='3'>Loding ....</td> ";
                    };
                    http.onload = function () {
                        if(this.readyState ==4 && this.status == 200){
                            todoData = JSON.parse(http.responseText);
                            userIdList(todoData);
                            // tableData(todoData);
                            table_body.innerHTML="<td class='loding-data' colspan='3'>Please Select userId</td> ";
                            
                        }
                    };
                    http.send();
                }; httpCall(urlTodos);
                
                
                
                
         // ------------ uniqueUserId List  fucntion --------
        function userIdList(todoData){
                    let todoDataLength = todoData.length;
                    let uniqueUserId = [];

                    for(var a=0; a < todoDataLength; a++){
                        if(uniqueUserId.indexOf(todoData[a].userId) === -1){
                            uniqueUserId.push(todoData[a].userId);
                        }
                    }
                    
                    let uniqueLength = uniqueUserId.length;
                    let userIdOption = "<option value='0'>select userId </option>";
                    for(var b= 0; b < uniqueLength; b++){
                        userIdOption +="<option value='"+uniqueUserId[b]+"'>"+uniqueUserId[b]+"</option>";
                    }
                    document.getElementById("userIdOption").innerHTML = userIdOption; 
            
        }; 


        
         // ------------ UserId  spesific  data   fucntion --------
            function idSpesificData(id){       
                        let todoDataLength = todoData.length;
                         spesificData = [];

                        for(var e=0; e < todoDataLength; e++){
                            if(todoData[e].userId == id){
                                spesificData.push(todoData[e]);
                            }
                        }
                        // -----------Table Append spesific data ---------
                       
                        tableData(spesificData);
                    
            };
            


            //------------------- USerid Select box   fucntion  ------------
            var userIdSelect =  document.getElementById("userIdOption");
            userIdSelect.addEventListener("change", userIdOnchange);
            
            function userIdOnchange(){
                let userIdValue =  document.getElementById("userIdOption").value;
                //--------idSpesificData ------- 
                idSpesificData(userIdValue);
                
                //-------- chart Update base on selection  ------- 
                let spesificDataLength = spesificData.length;
                document.getElementById("barChartWrp").innerHTML = " <canvas width='450' height='250' id='barChart'> </canvas>";
                barChart();
               
            }


            
            
            //-------------- Data  table append row -----------
        function tableData(todoData) {
            let todoDataLength = todoData.length;
            let body = document.getElementById("todoTable_body");
             body.innerHTML = "";
            
            for(var d=0; d < todoDataLength; d++){
                let row = body.insertRow(0);
                let cell1 = row.insertCell(0);
                let cell2 = row.insertCell(0);
                let cell3 = row.insertCell(0);
                //---------- create  checkBox ---------
                let checkBox1 = "<input type='checkbox' value='aaa'>"; 
                let checkBox = document.createElement("input"); 
                checkBox.setAttribute("type", "checkbox");
                checkBox.setAttribute("name", "dd");
                checkBox.setAttribute("value", "ff");
                checkBox.setAttribute("disabled", "disabled");
                let checkboxArgument = todoData[d].completed;
                let checkboxArgumentFun = function(arument){
                    if(arument == true){
                        return true;
                    }else {
                        return false;
                    }
                };;
                checkBox.checked = checkboxArgumentFun(checkboxArgument);

                cell1.appendChild(checkBox);
                cell3.innerHTML = todoData[d].id;
                cell2.innerHTML = todoData[d].title;
            }
        };
        var toggle = 0

        // ---------------- Table Sort fucntion -------------------
        document.getElementById("sortById").onclick= function() {
            if(typeof spesificData != 'undefined'){
                let ele = document.getElementById("id");
                //-- toggle -----------
                if(toggle ==0){
                    spesificData.sort(function(a,b){ return a.id-b.id});
                    tableData(spesificData);
                    toggle=1; 
                    ele.setAttribute("class","up");
                    console.log(toggle);
                }else {
                    spesificData.sort(function(a,b){ return b.id-a.id});
                    tableData(spesificData);
                    toggle=0; 
                    console.log(toggle);
                    ele.setAttribute("class","down");
                }
            }
        }
        
        
        document.getElementById("sortByTitle").onclick= function() {
            if(typeof spesificData != 'undefined'){
                let ele = document.getElementById("title");
                //-- toggle -----------
                if(toggle ==0){
                    function compare(a,b) {
                        if (a.title < b.title)
                        return -1;
                        if (a.title > b.title)
                        return 1;
                        return 0;
                    }
                    
                    
                    spesificData.sort(compare);
                    tableData(spesificData);
                    toggle=1; 
                    console.log(toggle);
                    ele.setAttribute("class","up");
                }else {
                    function compare(a,b) {
                        if (b.title < a.title)
                        return -1;
                        if (b.title > a.title)
                        return 1;
                        return 0;
                    }
                    spesificData.sort(compare);
                    tableData(spesificData);
                    toggle=0; 
                    console.log(toggle);
                    ele.setAttribute("class","down");
                }
            }
        }
        
        
        
        document.getElementById("sortByCompleted").onclick= function() {
            if(typeof spesificData != 'undefined'){
                let ele = document.getElementById("completed");
                //-- toggle -----------
                if(toggle ==0){
                    function compare(a,b) {
                        if (a.completed < b.completed)
                        return -1;
                        if (a.completed > b.completed)
                        return 1;
                        return 0;
                    }
                    
                    
                    spesificData.sort(compare);
                    tableData(spesificData);
                    toggle=1; 
                    ele.setAttribute("class","up");
                    console.log(toggle);
                }else {
                    function compare(a,b) {
                        if (b.completed < a.completed)
                        return -1;
                        if (b.completed > a.completed)
                        return 1;
                        return 0;
                    }
                    spesificData.sort(compare);
                    tableData(spesificData);
                    toggle=0; 
                    console.log(toggle);
                    ele.setAttribute("class","down");
                }
            }
        }
        
        
    
        // ------------------ Canvas bar chart fucntion -----------------

        function barChart(){
            //---------- Property 
            var cvs =  document.getElementById("barChart");
            let  width = 450;
            let  height = 250;
            let width10 = width/18;
            let width100 = width/100;
            let distance = width100*10;
            let startX = distance;
            let startY = 0;
            

            cvs.setAttribute("width", width);
            cvs.setAttribute("height",height);
            var ctx = cvs.getContext("2d");
           
             //-------get chart count  completed and incomplated -----------
             function chartCount(data,val){
                let count =0;
                let len = data.length;
                for(var f=0; f < len; f++){
                    if(data[f].completed == val){
                        count++;
                        }
                }
                console.log("data",f,data,count);
                return count;
            } 
            
            //------ Draw Grid fucntion ---------------------
            function drawGrid(w, h) {
                for(var y=0; y<10; y++){
                    ctx.beginPath();
                    ctx.moveTo(25, width10*y);
                    ctx.lineTo(w, width10*y);
                    ctx.strokeStyle = "#f7f6f6";
                    ctx.stroke();
                    ctx.closePath();
                }
            }; drawGrid(width, height);
            

            //------ Y lable fucntion ---------------------
                function ylable(h){
                    let Ylable = [0,10,20,30,40,50,60,70,80,90,100];
                    Ylable.reverse();
                    for(var y=0; y < 11; y++){
                        ctx.beginPath();
                        ctx.font = "10px Arial";
                        ctx.fillStyle = "#a09b9b";
                        ctx.textAlign = "right";
                        if(y == 0){
                            console.log(y,">>>>>>>>>>>>>>>>", Ylable[y])
                            ctx.fillText(Ylable[y], 20, 7);
                            
                        }else {
                            ctx.fillText(Ylable[y], 20, width10*y);

                        }
                        ctx.closePath();
                    }
                }ylable();
                
                chartLegent(50,20, 10,10);
                ctx.translate(0, height);
                ctx.scale(1, -1);
                
                //------- Draw Bar chart rect --------------------
                function drawRect(ctx, startX, startY, height){
                let completed = "red";
                let incomplted = "green";
                rectWidth= 100;
                let presentHeightA = (height /100)* ( (chartCount(spesificData, false)) / spesificData.length * 100); 
                let presentHeightB = (height /100)* ( (chartCount(spesificData, true)) / spesificData.length * 100); 
                
                        for(var d=0; d < 2; d++){
                            
                            ctx.beginPath();
                            if(d %2 == 0){
                                ctx.rect(startX*4, startY, rectWidth, presentHeightA );
                                ctx.fillStyle = completed;
                            }else {
                                ctx.rect(startX, startY, rectWidth, presentHeightB );
                                ctx.fillStyle = incomplted;
                            }
                            ctx.stroke();
                            ctx.fill();
                            ctx.closePath();
                        
                        }
                }

                // ---------- chart bar  append times ----------
                    drawRect(ctx, startX, startY, height);

                
                // ---------------- chart Legent fucntion ----
                function chartLegent (startX, startY, rectWidth, rectHeight){
             
                    let legent = [
                                    {
                                    name:"completed",
                                    color:"green"
                                    },
                                    {
                                        name:"incompleted",
                                        color:"red"
                                    },
                                ];

                    
                        //--- Complated ----------
                        ctx.beginPath();
                        ctx.rect(startX, startY, rectWidth,rectHeight);
                        ctx.fillStyle = legent[0].color; 
                        ctx.stroke();
                        ctx.fill();
                        ctx.closePath();
                        ctx.beginPath();
                        ctx.font = "10px Arial";
                        ctx.fillStyle = "#a09b9b";
                        ctx.textAlign = "right";
                        ctx.fillText(legent[0].name, startX+60, startY+8);

                        //--- InComplated ----------
                        ctx.beginPath();
                        ctx.rect(startX+80, startY, rectWidth,rectHeight);
                        ctx.fillStyle = legent[1].color; 
                        ctx.stroke();
                        ctx.fill();
                        ctx.closePath();
                        ctx.beginPath();
                        ctx.font = "10px Arial";
                        ctx.fillStyle = "#a09b9b";
                        ctx.textAlign = "right";
                        ctx.fillText(legent[1].name, startX+150, startY+8);

                }  

           
                

        }; 




    





    };


